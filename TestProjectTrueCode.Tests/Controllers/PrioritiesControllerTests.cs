﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System.Diagnostics;
using TestProjectTrueCode.Controllers;
using TestProjectTrueCode.Data;
using Xunit;
using static TestProjectTrueCode.Controllers.PrioritiesController;

namespace TestProjectTrueCode.Tests.Controllers;

public class PrioritiesControllerTests
{
    private AppDbContext _context;
    private PrioritiesController _controller;

    public PrioritiesControllerTests()
    {
        var serviceProvider = new ServiceCollection()
            .AddEntityFrameworkInMemoryDatabase()
            .BuildServiceProvider();

        var builder = new DbContextOptionsBuilder<AppDbContext>()
            .UseInMemoryDatabase(databaseName: "test_db")
            .UseInternalServiceProvider(serviceProvider);

        _context = new AppDbContext(builder.Options);

        SeedData();

        _controller = new PrioritiesController(_context);
    }

    private void SeedData()
    {
        _context.Priorities.AddRange(
            new Priority { Id = 1, Level = 1 },
            new Priority { Id = 2, Level = 2 }
            );
        _context.SaveChanges();
    }

    [Fact]
    public async Task GetPriorities_ReturnsAllPriorities()
    {
        var result = await _controller.GetPriorities();

        var actionResult = Assert.IsType<ActionResult<IEnumerable<PriorityDto>>>(result);
        var returnValue = Assert.IsType<List<PriorityDto>>(actionResult.Value);
        Assert.Equal(2, returnValue.Count);
    }

    [Fact]
    public async Task GetPriorities_ReturnsAllPriorities2()
    {
        var result = await _controller.GetPriorities();

        var actionResult = Assert.IsType<ActionResult<IEnumerable<PriorityDto>>>(result);
        var returnValue = Assert.IsType<List<PriorityDto>>(actionResult.Value);
        Assert.Equal(2, returnValue.Count);
    }

    [Fact]
    public async Task GetPriority_ReturnsPriorityById()
    {
        var result = await _controller.GetPriority(1);

        var actionResult = Assert.IsType<ActionResult<PriorityDto>>(result);
        var returnValue = Assert.IsType<PriorityDto>(actionResult.Value);
        Assert.Equal(1, returnValue.Id);
    }

    [Fact]
    public async Task GetPriority_ReturnsNotFound()
    {
        var result = await _controller.GetPriority(3);

        Assert.IsType<NotFoundResult>(result.Result);
    }

    [Fact]
    public async Task PutPriority_UpdatesPriority()
    {
        var priority = new Priority { Id = 1, Level = 2 };
        var existingPriority = await _context.Priorities.FindAsync(1);
        _context.Entry(existingPriority!).State = EntityState.Detached;

        var result = await _controller.PutPriority(1, priority);

        Assert.IsType<OkResult>(result);
        var updatedPriority = await _context.Priorities.FindAsync(1);
        Assert.Equal(2, updatedPriority?.Level);
    }

    [Fact]
    public async Task PostPriority_CreatesPriority()
    {
        var priority = new Priority { Id = 3, Level = 3 };

        var result = await _controller.PostPriority(priority);

        var createdPriority = await _context.Priorities.FindAsync(3);
        Assert.NotNull(createdPriority);
        Assert.IsType<OkResult>(result);
    }

    [Fact]
    public async Task DeletePriority_RemovesPriority()
    {
        var result = await _controller.DeletePriority(1);

        var deletedPriority = await _context.Priorities.FindAsync(1);
        Assert.Null(deletedPriority);
        Assert.IsType<OkResult>(result);
    }
}
