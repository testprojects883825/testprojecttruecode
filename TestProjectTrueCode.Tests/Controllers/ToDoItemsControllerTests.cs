﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System.Diagnostics;
using TestProjectTrueCode.Controllers;
using TestProjectTrueCode.Data;
using Xunit;
using static TestProjectTrueCode.Controllers.ToDoItemsController;

namespace TestProjectTrueCode.Tests.Controllers;

public class ToDoItemsControllerTests
{
    private AppDbContext _context;
    private ToDoItemsController _controller;

    public ToDoItemsControllerTests()
    {
        var serviceProvider = new ServiceCollection()
            .AddEntityFrameworkInMemoryDatabase()
            .BuildServiceProvider();

        var builder = new DbContextOptionsBuilder<AppDbContext>()
            .UseInMemoryDatabase(databaseName: "test_db")
            .UseInternalServiceProvider(serviceProvider);

        _context = new AppDbContext(builder.Options);
        
        SeedData();

        _controller = new ToDoItemsController(_context);
    }

    private void SeedData()
    {
        var user = new User { Id = 1, Name = "Test User" };
        var priority = new Priority { Id = 1, Level = 1 };

        _context.Users.Add(user);
        _context.Priorities.Add(priority);
        _context.ToDoItems.AddRange(
            new ToDoItem { Id = 1, Title = "Task 1", Description = "Description 1", IsCompleted = false, DueDate = DateTime.UtcNow.AddDays(1), PriorityId = 1, UserId = 1 },
            new ToDoItem { Id = 2, Title = "Task 2", Description = "Description 2", IsCompleted = true, DueDate = DateTime.UtcNow.AddDays(2), PriorityId = 1, UserId = 1 }
        );
        _context.SaveChanges();
    }

    [Fact]
    public async Task GetToDoItems_ReturnsAllItems()
    {
        var result = await _controller.GetToDoItems(null, null);

        var actionResult = Assert.IsType<ActionResult<IEnumerable<ToDoItemDto>>>(result);
        var returnValue = Assert.IsType<List<ToDoItemDto>>(actionResult.Value);
        Assert.Equal(2, returnValue.Count);
    }

    [Fact]
    public async Task GetToDoItem_ReturnsNotFound_ForInvalidId()
    {
        var result = await _controller.GetToDoItem(99);

        Assert.IsType<NotFoundResult>(result.Result);
    }

    [Fact]
    public async Task GetToDoItem_ReturnsCorrectItem_ForValidId()
    {
        var result = await _controller.GetToDoItem(1);

        var actionResult = Assert.IsType<ActionResult<ToDoItemDto>>(result);
        var returnValue = Assert.IsType<ToDoItemDto>(actionResult.Value);
        Assert.Equal(1, returnValue.Id);
    }

    [Fact]
    public async Task PutToDoItem_UpdatesItem()
    {
        var toDoItem = new ToDoItem { Id = 1, Title = "Updated Task", Description = "Updated Description", IsCompleted = true, DueDate = DateTime.UtcNow.AddDays(1), PriorityId = 1, UserId = 1 };
        var existingPriority = await _context.ToDoItems.FindAsync(1);
        _context.Entry(existingPriority!).State = EntityState.Detached;

        var result = await _controller.PutToDoItem(1, toDoItem);

        Assert.IsType<OkResult>(result);
        var updatedItem = await _context.ToDoItems.FindAsync(1);
        Assert.Equal("Updated Task", updatedItem!.Title);
        Assert.Equal("Updated Description", updatedItem.Description);
        Assert.True(updatedItem.IsCompleted);
    }

    [Fact]
    public async Task PostToDoItem_CreatesNewItem()
    {
        var newToDoItem = new ToDoItem { Id = 3, Title = "New Task", Description = "New Description", IsCompleted = false, DueDate = DateTime.UtcNow.AddDays(3), PriorityId = 1, UserId = 1 };

        var result = await _controller.PostToDoItem(newToDoItem);

        Assert.IsType<OkResult>(result);
        var createdItem = await _context.ToDoItems.FindAsync(3);
        Assert.NotNull(createdItem);
        Assert.Equal("New Task", createdItem.Title);
    }

    [Fact]
    public async Task DeleteToDoItem_DeletesItem()
    {
        var result = await _controller.DeleteToDoItem(1);

        Assert.IsType<OkResult>(result);
        var deletedItem = await _context.ToDoItems.FindAsync(1);
        Assert.Null(deletedItem);
    }

    [Fact]
    public async Task AssignUser_AssignsUserToItem()
    {
        var result = await _controller.AssignUser(1, 1);

        Assert.IsType<OkResult>(result);
        var assignedItem = await _context.ToDoItems.FindAsync(1);
        Assert.Equal(1, assignedItem!.UserId);
    }
}
