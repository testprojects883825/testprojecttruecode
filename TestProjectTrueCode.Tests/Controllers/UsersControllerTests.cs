﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using TestProjectTrueCode.Controllers;
using TestProjectTrueCode.Data;
using Xunit;
using static TestProjectTrueCode.Controllers.UsersController;

namespace TestProjectTrueCode.Tests.Controllers;

public class UsersControllerTests
{
    private AppDbContext _context;
    private UsersController _controller;

    public UsersControllerTests()
    {
        var serviceProvider = new ServiceCollection()
            .AddEntityFrameworkInMemoryDatabase()
            .BuildServiceProvider();

        var builder = new DbContextOptionsBuilder<AppDbContext>()
            .UseInMemoryDatabase(databaseName: "test_db")
            .UseInternalServiceProvider(serviceProvider);

        _context = new AppDbContext(builder.Options);

        SeedData();

        _controller = new UsersController(_context);
    }

    private void SeedData()
    {
        _context.Users.AddRange(
            new User { Id = 1, Name = "User1" },
            new User { Id = 2, Name = "User2" }
        );
        _context.SaveChanges();
    }

    [Fact]
    public async Task GetUsers_ReturnsAllUsers()
    {
        var result = await _controller.GetUsers();

        var actionResult = Assert.IsType<ActionResult<IEnumerable<UserDto>>>(result);
        var returnValue = Assert.IsType<List<UserDto>>(actionResult.Value);
        Assert.Equal(2, returnValue.Count);
    }

    [Fact]
    public async Task GetUser_ReturnsNotFound_ForInvalidId()
    {
        var result = await _controller.GetUser(99);

        Assert.IsType<NotFoundResult>(result.Result);
    }

    [Fact]
    public async Task GetUser_ReturnsCorrectUser_ForValidId()
    {
        var result = await _controller.GetUser(1);

        var actionResult = Assert.IsType<ActionResult<UserDto>>(result);
        var returnValue = Assert.IsType<UserDto>(actionResult.Value);
        Assert.Equal(1, returnValue.Id);
    }

    [Fact]
    public async Task PutUser_UpdatesUser()
    {
        var user = new User { Id = 1, Name = "Updated User" };
        var existingPriority = await _context.Users.FindAsync(1);
        _context.Entry(existingPriority!).State = EntityState.Detached;

        var result = await _controller.PutUser(1, user);

        Assert.IsType<OkResult>(result);
        var updatedUser = await _context.Users.FindAsync(1);
        Assert.Equal("Updated User", updatedUser!.Name);
    }

    [Fact]
    public async Task PostUser_CreatesNewUser()
    {
        var newUser = new User { Id = 3, Name = "New User" };

        var result = await _controller.PostUser(newUser);

        Assert.IsType<OkResult>(result);
        var createdUser = await _context.Users.FindAsync(3);
        Assert.NotNull(createdUser);
        Assert.Equal("New User", createdUser.Name);
    }

    [Fact]
    public async Task PostUser_ReturnsBadRequest_ForInvalidUserName()
    {
        var newUser = new User { Id = 3, Name = "AdminUser" };

        var result = await _controller.PostUser(newUser);

        var badRequestResult = Assert.IsType<BadRequestObjectResult>(result);
        var errors = Assert.IsType<SerializableError>(badRequestResult.Value);
        Assert.True(errors.ContainsKey("Name"));
    }

    [Fact]
    public async Task DeleteUser_DeletesUser()
    {
        var result = await _controller.DeleteUser(1);

        Assert.IsType<OkResult>(result);
        var deletedUser = await _context.Users.FindAsync(1);
        Assert.Null(deletedUser);
    }
}
