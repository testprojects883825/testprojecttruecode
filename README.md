# Тестовое задание для true.code
## Кейс:
#### Описание системы
Создать веб-приложение для управления списком задач (ToDo list). Предоставить Web API для создания, просмотра, редактирования и удаления задач, а также управление связанными данными.
#### Модель:
ToDoItem:
Id
Title
Description
IsCompleted
DueDate
Priority
User

 Priority:
Level (int)
ToDoItems

User:
Id
Name
ToDoItems
#### Требования:
.NET 6+ ASP.NET & EF
Database (any SQL)
Реализовать валидацию входных данных
Реализовать обработку ошибок
Реализовать фильтрацию задач по статусу (выполненные/невыполненные) и приоритету
Реализовать возможность назначения задачи пользователю

## Инструкция по запуску проекта

### Запущенный проект на render.com
SwaggerUI на запущенном проекте доступен по ссылке: https://testprojecttruecode4.onrender.com/swagger/index.html.
Первый запрос может длится до 30-40 секунд, т.к. контейнер автоматически останавливается при бездействии.

### Запуск проекта на локальной машине

#### Клонировать проект
```
git clone https://gitlab.com/testprojects883825/testprojecttruecode.git
```
#### Запуск проекта
Так как по умолчанию установлено окружение Development, то после запуска проекта должна открыться страница SwaggerUI (https://localhost:7178/swagger/index.html). Таким образом можно протестировать API.

PostgreSQL БД крутится на render.com.

#### Подключение другой БД
Если по какой-то причине необходимо подключить приложение к другой БД, то следует внести изменения в параметры подключения в файле /Data/AppDbContext.cs, указав настройки целевой БД:
```C#
protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
{
    optionsBuilder.UseNpgsql(
        "Host=dpg-cq5o6buehbks73brrk50-a.frankfurt-postgres.render.com;" +
        "Port=5432;" +
        "Database=testprojecttruecode;" +
        "Username=dexogen;" +
        "Password=Fodde95lSDFLcD5AnpRdTCBedT9RAZX8");
}
```
А также не забыть применить миграции командой Update-database в консоли диспетчера пакетов.

#### Юнит тесты
Юнит тесты контроллеров реализованы с применением xUnit. Тесты находятся в отдельном проекте TestProjectTrueCode.Tests.
