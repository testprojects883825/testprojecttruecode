using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TestProjectTrueCode.Data;

namespace TestProjectTrueCode.Controllers;

[Route("api/[controller]")]
[ApiController]
public class PrioritiesController : ControllerBase
{
    private readonly AppDbContext _context;

    public PrioritiesController(AppDbContext context)
    {
        _context = context;
    }

    [HttpGet]
    public async Task<ActionResult<IEnumerable<PriorityDto>>> GetPriorities()
    {
        return await _context.Priorities
            .Select(p => new PriorityDto(p.Id, p.Level))
            .ToListAsync();
    }

    [HttpGet("{id}")]
    public async Task<ActionResult<PriorityDto>> GetPriority(int id)
    {
        var priority = await _context.Priorities
            .FirstOrDefaultAsync(p => p.Id == id);

        if (priority == null)
        {
            return NotFound();
        }

        return new PriorityDto(priority.Id, priority.Level);
    }

    [HttpPut("{id}")]
    public async Task<IActionResult> PutPriority(int id, Priority priority)
    {
        if (!ModelState.IsValid)
        {
            return BadRequest(ModelState);
        }

        priority.Id = id;
        _context.Entry(priority).State = EntityState.Modified;

        try
        {
            await _context.SaveChangesAsync();
        }
        catch (DbUpdateConcurrencyException)
        {
            if (!PriorityExists(id))
            {
                return NotFound();
            }
            else
            {
                throw;
            }
        }

        return Ok();
    }

    [HttpPost]
    public async Task<IActionResult> PostPriority(Priority priority)
    {
        if (!ModelState.IsValid)
        {
            return BadRequest(ModelState);
        }

        _context.Priorities.Add(priority);
        await _context.SaveChangesAsync();

        return Ok();
    }

    [HttpDelete("{id}")]
    public async Task<IActionResult> DeletePriority(int id)
    {
        var priority = await _context.Priorities.FindAsync(id);
        if (priority == null)
        {
            return NotFound();
        }

        _context.Priorities.Remove(priority);
        await _context.SaveChangesAsync();

        return Ok();
    }

    private bool PriorityExists(int id)
    {
        return _context.Priorities.Any(e => e.Id == id);
    }

    public record PriorityDto(int Id, int Level);
}
