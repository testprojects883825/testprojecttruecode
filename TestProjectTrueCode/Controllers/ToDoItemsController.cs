using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TestProjectTrueCode.Data;

namespace TestProjectTrueCode.Controllers;

[Route("api/[controller]")]
[ApiController]
public class ToDoItemsController : ControllerBase
{
    private readonly AppDbContext _context;

    public ToDoItemsController(AppDbContext context)
    {
        _context = context;
    }

    [HttpGet]
    public async Task<ActionResult<IEnumerable<ToDoItemDto>>> GetToDoItems([FromQuery] bool? isCompleted, [FromQuery] int? priorityLevel)
    {
        var query = _context.ToDoItems
            .Include(t => t.Priority)
            .Include(t => t.User)
            .AsQueryable();

        if (isCompleted.HasValue)
        {
            query = query.Where(t => t.IsCompleted == isCompleted.Value);
        }

        if (priorityLevel.HasValue)
        {
            query = query.Where(t => t.Priority!.Level == priorityLevel.Value);
        }

        return await query
            .Select(t => new ToDoItemDto(
                t.Id, 
                t.Title, 
                t.Description, 
                t.IsCompleted, 
                t.DueDate,
                t.Priority != null ? t.Priority.Level : 0,
                t.User != null ? t.User.Id : 0))
            .ToListAsync();
    }

    [HttpGet("{id}")]
    public async Task<ActionResult<ToDoItemDto>> GetToDoItem(int id)
    {
        var toDoItem = await _context.ToDoItems
            .Include(t => t.Priority)
            .Include(t => t.User)
            .FirstOrDefaultAsync(t => t.Id == id);

        if (toDoItem == null)
        {
            return NotFound();
        }

        return new ToDoItemDto(
            toDoItem.Id,
            toDoItem.Title,
            toDoItem.Description,
            toDoItem.IsCompleted,
            toDoItem.DueDate,
            toDoItem.Priority != null ? toDoItem.Priority.Id : 0,
            toDoItem.User != null ? toDoItem.User.Id : 0);
    }

    [HttpPut("{id}")]
    public async Task<IActionResult> PutToDoItem(int id, ToDoItem toDoItem)
    {
        if (!ModelState.IsValid)
        {
            return BadRequest(ModelState);
        }

        toDoItem.Id = id;
        _context.Entry(toDoItem).State = EntityState.Modified;

        try
        {
            await _context.SaveChangesAsync();
        }
        catch (DbUpdateConcurrencyException)
        {
            if (!ToDoItemExists(id))
            {
                return NotFound();
            }
            else
            {
                throw;
            }
        }

        return Ok();
    }

    [HttpPost]
    public async Task<IActionResult> PostToDoItem(ToDoItem toDoItem)
    {
        if (!_context.Priorities.Any(p => p.Id == toDoItem.PriorityId))
        {
            toDoItem.PriorityId = null;
        }

        if (!_context.Users.Any(u => u.Id == toDoItem.UserId))
        {
            toDoItem.UserId = null;
        }

        if (!ModelState.IsValid)
        {
            return BadRequest(ModelState);
        }

        _context.ToDoItems.Add(toDoItem);
        await _context.SaveChangesAsync();

        return Ok();
    }

    [HttpDelete("{id}")]
    public async Task<IActionResult> DeleteToDoItem(int id)
    {
        var toDoItem = await _context.ToDoItems.FindAsync(id);
        if (toDoItem == null)
        {
            return NotFound();
        }

        _context.ToDoItems.Remove(toDoItem);
        await _context.SaveChangesAsync();

        return Ok();
    }

    [HttpPut("{id}/AssignUser/{userId}")]
    public async Task<IActionResult> AssignUser(int id, int userId)
    {
        var toDoItem = await _context.ToDoItems.FindAsync(id);
        if (toDoItem == null)
        {
            return NotFound();
        }

        var user = await _context.Users.FindAsync(userId);
        if (user == null)
        {
            return NotFound();
        }

        toDoItem.UserId = userId;
        _context.Entry(toDoItem).State = EntityState.Modified;

        try
        {
            await _context.SaveChangesAsync();
        }
        catch (DbUpdateConcurrencyException)
        {
            if (!ToDoItemExists(id))
            {
                return NotFound();
            }
            else
            {
                throw;
            }
        }

        return Ok();
    }

    private bool ToDoItemExists(int id)
    {
        return _context.ToDoItems.Any(e => e.Id == id);
    }

    public record ToDoItemDto(int Id, string? Title, string? Description, 
        bool IsCompleted, DateTime DueDate, int PriorityId, int UserId);
}
