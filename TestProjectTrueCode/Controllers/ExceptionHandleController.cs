using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using Swashbuckle.AspNetCore.Annotations;
using System.Diagnostics;

namespace TestProjectTrueCode.Controllers;

[Route("api/[controller]")]
[ApiController]
[AllowAnonymous]
public class ExceptionHandleController : ControllerBase
{
    [SwaggerIgnore]
    public IActionResult ProccessException([FromServices] IHostEnvironment hostEnvironment)
    {
        if (hostEnvironment.IsProduction())
        {
            return Problem();
        }

        var feature = HttpContext.Features.Get<IExceptionHandlerFeature>();
        return Problem(
            instance: hostEnvironment.EnvironmentName,
            detail: feature?.Error.StackTrace,
            title: feature?.Error.Message,
            type: feature?.Error.Source);
    }

    [HttpGet("ThrowTestException")]
    public IActionResult ThrowException()
    {
        throw new UnreachableException("TestException!");
    }
}
