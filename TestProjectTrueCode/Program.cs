using TestProjectTrueCode.Data;

namespace TestProjectTrueCode;

public class Program
{
    public static void Main(string[] args)
    {
        var builder = WebApplication.CreateBuilder(args);

        builder.Services.AddControllers();

        builder.Services.AddSwaggerGen(options =>
        {
            options.EnableAnnotations();
        });
        builder.Services.AddDbContext<AppDbContext>();

        var app = builder.Build();

        //if (!app.Environment.IsProduction())
        {
            app.UseSwagger();
            app.UseSwaggerUI();
        }

        app.UseExceptionHandler("/api/ExceptionHandle");

        app.MapControllers();

        app.Run();
    }
}
