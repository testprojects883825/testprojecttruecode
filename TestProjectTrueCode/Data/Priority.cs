﻿using Swashbuckle.AspNetCore.Annotations;
using System.ComponentModel.DataAnnotations;

namespace TestProjectTrueCode.Data;

public class Priority
{
    [SwaggerIgnore]
    public int Id { get; set; }

    [Required(ErrorMessage = "Please enter priority level")]
    [Range(1, 100)]
    public int Level { get; set; }

    public List<ToDoItem> ToDoItems { get; private set; } = new();
}
