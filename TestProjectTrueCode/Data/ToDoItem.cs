﻿using Swashbuckle.AspNetCore.Annotations;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TestProjectTrueCode.Data;

public record ToDoItem
{
    [SwaggerIgnore]
    public int Id { get; set; }

    [Required(ErrorMessage = "Please enter ToDoItem title")]
    [StringLength(50, MinimumLength = 1)]
    public string? Title { get; set; }

    [StringLength(400, MinimumLength = 1)]
    public string? Description { get; set; }

    public bool IsCompleted { get; set; }

    public DateTime DueDate { get; set; }

    public int? PriorityId { get; set; }

    [SwaggerIgnore]
    public Priority? Priority { get; set; }

    public int? UserId { get; set; }

    [SwaggerIgnore]
    public User? User { get; set; }
}
