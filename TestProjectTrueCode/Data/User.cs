﻿using System.ComponentModel.DataAnnotations;
using Swashbuckle.AspNetCore.Annotations;

namespace TestProjectTrueCode.Data;

public class User
{
    [SwaggerIgnore]
    public int Id { get; set; }

    [Required(ErrorMessage = "Please enter username")]
    [StringLength(50, MinimumLength = 3)]
    public string? Name { get; set; }

    public List<ToDoItem> ToDoItems { get; private set; } = new();
}
