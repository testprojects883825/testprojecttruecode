﻿using Microsoft.EntityFrameworkCore;

namespace TestProjectTrueCode.Data;

public class AppDbContext : DbContext
{
    public DbSet<ToDoItem> ToDoItems { get; set; }
    public DbSet<Priority> Priorities { get; set; }
    public DbSet<User> Users { get; set; }

    public AppDbContext() { }

    public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
    {
        //Database.EnsureCreated();
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
        optionsBuilder.UseNpgsql(
            "Host=dpg-cq5o6buehbks73brrk50-a.frankfurt-postgres.render.com;" +
            "Port=5432;" +
            "Database=testprojecttruecode;" +
            "Username=dexogen;" +
            "Password=Fodde95lSDFLcD5AnpRdTCBedT9RAZX8");
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);

        // Users test data
        modelBuilder.Entity<User>().HasData(
            new User { Id = 1, Name = "Vasya Pupkin" },
            new User { Id = 2, Name = "Valera Krilov" },
            new User { Id = 3, Name = "Irina Tarasova" }
        );

        // Priorities test data
        modelBuilder.Entity<Priority>().HasData(
            new Priority { Id = 1, Level = 1 },
            new Priority { Id = 2, Level = 2 },
            new Priority { Id = 3, Level = 3 }
        );

        // ToDoItems test data
        modelBuilder.Entity<ToDoItem>().HasData(
            new ToDoItem
            {
                Id = 1,
                Title = "Complete assignment",
                Description = "Finish the project assignment for the client",
                IsCompleted = false,
                DueDate = DateTime.Now.AddDays(2).ToUniversalTime(),
                PriorityId = 3,
                UserId = 3
            },
            new ToDoItem
            {
                Id = 2,
                Title = "Buy goods",
                Description = "Milk, Bread, Eggs, Butter",
                IsCompleted = false,
                DueDate = DateTime.Now.AddDays(1).ToUniversalTime(),
                PriorityId = 1,
                UserId = 2
            },
            new ToDoItem
            {
                Id = 3,
                Title = "Schedule meeting",
                Description = "Set up a meeting with the project team",
                IsCompleted = true,
                DueDate = DateTime.Now.AddDays(5).ToUniversalTime(),
                PriorityId = 3,
                UserId = 2
            }
        );
    }
}
